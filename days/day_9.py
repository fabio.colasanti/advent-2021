# https://adventofcode.com/2021/day/8
import math
from typing import List, Dict, Tuple


def get_neighbouring_values(coord: Tuple[int, int], heightmap) -> List[Tuple[int, int]]:
    x = coord[0]
    y = coord[1]

    max_x = max([x[0] for x in heightmap.keys()])
    max_y = max([x[1] for x in heightmap.keys()])

    neighbours_values = []

    if x == 0:
        if y == 0:
            neighbours_values.append((x + 1, y))
            neighbours_values.append((x, y + 1))
        elif y == max_y:
            neighbours_values.append((x + 1, y))
            neighbours_values.append((x, y - 1))
        else:
            neighbours_values.append((x + 1, y))
            neighbours_values.append((x, y + 1))
            neighbours_values.append((x, y - 1))
    elif x == max_x:
        if y == 0:
            neighbours_values.append((x - 1, y))
            neighbours_values.append((x, y + 1))
        elif y == max_y:
            neighbours_values.append((x - 1, y))
            neighbours_values.append((x, y - 1))
        else:
            neighbours_values.append((x, y + 1))
            neighbours_values.append((x, y - 1))
            neighbours_values.append((x - 1, y))
    else:
        if y == 0:
            neighbours_values.append((x - 1, y))
            neighbours_values.append((x + 1, y))
            neighbours_values.append((x, y + 1))
        elif y == max_y:
            neighbours_values.append((x - 1, y))
            neighbours_values.append((x + 1, y))
            neighbours_values.append((x, y - 1))
        else:
            neighbours_values.append((x, y + 1))
            neighbours_values.append((x, y - 1))
            neighbours_values.append((x + 1, y))
            neighbours_values.append((x - 1, y))

    return neighbours_values


def get_min_heights(heightmap: Dict) -> Tuple[List[Tuple], List[int]]:
    min_heights_coords = []
    min_heights_values = []

    for coord, height in heightmap.items():
        neighbouring_values = get_neighbouring_values(coord, heightmap)
        if height < min([heightmap[x] for x in neighbouring_values]):
            min_heights_coords.append(coord)
            min_heights_values.append(height + 1)

    return min_heights_coords, min_heights_values


def parse_input(input: List[str]) -> Dict:
    heightmap = {}

    for y, line in enumerate(input):
        for x, value in enumerate(line):
            heightmap[(x, y)] = int(value)

    return heightmap


def get_basin(coord: Tuple[int, int], heightmap: Dict) -> List[int]:
    basin = [coord]

    for c in basin:
        neighbours = get_neighbouring_values(c, heightmap)
        for n in neighbours:
            if heightmap[n] != 9 and len([x for x in basin if x == n]) == 0:
                basin.append(n)

    return [heightmap[x] for x in basin]


if __name__ == '__main__':
    with open('../data/9.txt') as f:
        input = [x.strip() for x in f]

    heightmap = parse_input(input)
    min_heights_coords, min_heights_values = get_min_heights(heightmap)

    print(sum(min_heights_values))  # 545

    basins = [len(get_basin(x, heightmap)) for x in min_heights_coords]
    biggest_basins = sorted(basins, reverse=True)[:3]
    print(math.prod(biggest_basins))  # 950600
