# https://adventofcode.com/2021/day/8

from typing import List, Dict


def count_easy_digits(output: List[str]) -> int:
    return sum([1 for x in output if len(x) in [2, 3, 4, 7]])


def map_signals_to_digits(signal: List[str]) -> Dict:
    mapping = {
        1: {letter for letter in [segment for segment in signal if len(segment) == 2][0]},
        4: {letter for letter in [segment for segment in signal if len(segment) == 4][0]},
        7: {letter for letter in [segment for segment in signal if len(segment) == 3][0]},
        8: {letter for letter in [segment for segment in signal if len(segment) == 7][0]}
    }

    for segment in [set(letter) for letter in [segment for segment in signal if len(segment) == 5]]:
        if len(segment.intersection(mapping[1])) == 2:
            mapping[3] = segment
        elif len(segment.intersection(mapping[1])) == 1 and len(segment.intersection(mapping[4])) == 3:
            mapping[5] = segment
        else:
            mapping[2] = segment

    for segment in [set(letter) for letter in [segment for segment in signal if len(segment) == 6]]:
        if len(segment.intersection(mapping[4])) == 4:
            mapping[9] = segment
        elif len(segment.intersection(mapping[1])) == 2:
            mapping[0] = segment
        else:
            mapping[6] = segment

    return mapping


def decode_output(output: List[str], mapping: Dict) -> int:
    digits = []

    for segment in output:
        digit = [k for k, v in mapping.items() if set(segment) == v][0]
        digits.append(digit)

    return int(''.join([str(x) for x in digits]))


def decode_all(signals: List[List[str]], outputs: List[List[str]]) -> List[int]:
    values = []

    for i, signal in enumerate(signals):
        mapping = map_signals_to_digits(signal)
        value = decode_output(outputs[i], mapping)

        values.append(value)

    return values


if __name__ == '__main__':
    with open('../data/8.txt') as f:
        input = [x.strip().split(' | ') for x in f]
        signals = [x[0].split(' ') for x in input]
        outputs = [x[1].split(' ') for x in input]

    print(sum([count_easy_digits(o) for o in outputs]))  # 278

    print(sum(decode_all(signals, outputs)))  # 986179
