# https://adventofcode.com/2021/day/10

from typing import List, Tuple

DELIMITERS = {
    '(': ')',
    '[': ']',
    '{': '}',
    '<': '>',
}

POINTS_FOR_CORRUPTED = {
    ')': 3,
    ']': 57,
    '}': 1197,
    '>': 25137
}

POINTS_FOR_AUTOCOMPLETE = {
    ')': 1,
    ']': 2,
    '}': 3,
    '>': 4
}


def is_corrupted(line: str) -> Tuple[bool, int]:
    corrupted = False
    points = 0
    open = []

    for i, char in enumerate(line):
        if char in DELIMITERS.keys():
            open.append(char)
        else:
            if char == DELIMITERS.get(open[-1]):
                open.pop()
            else:
                points = POINTS_FOR_CORRUPTED.get(char)
                corrupted = True
                break

    return corrupted, points


def autocomplete(line: str) -> Tuple[str, int]:
    points = 0
    open = []

    for i, char in enumerate(line):
        if char in DELIMITERS.keys():
            open.append(char)
        else:
            if char == DELIMITERS.get(open[-1]):
                open.pop()

    missing = [DELIMITERS[x] for x in open]
    missing.reverse()

    for char in missing:
        points = points * 5 + POINTS_FOR_AUTOCOMPLETE[char]

    return ''.join(missing), points


def remove_corrupted(lines: List[str]) -> Tuple[List[str], int]:
    tot_points = 0
    uncorrupted_lines = []

    for line in lines:
        corrupted, points = is_corrupted(line)
        if corrupted:
            tot_points += points
        else:
            uncorrupted_lines.append(line)

    return uncorrupted_lines, tot_points


if __name__ == '__main__':
    with open('../data/10.txt') as f:
        lines = [x.strip() for x in f]

    uncorrupted_lines, points_for_corrupted = remove_corrupted(lines)
    print(points_for_corrupted)  # 392139

    all_autocomplete = []
    for line in uncorrupted_lines:
        all_autocomplete.append(autocomplete(line))

    middle = int(len(all_autocomplete) / 2)
    points_for_autocomplete = sorted([x[1] for x in all_autocomplete])[middle:middle + 1][0]

    print(points_for_autocomplete)  # 4001832844
