# https://adventofcode.com/2021/day/3
from copy import copy
from typing import List, Tuple


def get_indicators_part1(report: List[str]) -> Tuple[str, str]:
    gamma = ''
    epsilon = ''

    cols = len(report[0])
    rows = len(report)

    for i in range(cols):
        if sum([1 for row in report if row[i] == '1']) > rows / 2:
            gamma += '1'
            epsilon += '0'
        else:
            gamma += '0'
            epsilon += '1'

    return gamma, epsilon


def get_indicators_part2(report: List[str]) -> Tuple[str, str]:
    candidates = copy(report)
    i = 0
    while len(candidates) > 1:

        if sum([1 for row in candidates if row[i] == '1']) >= len(candidates) / 2:
            candidates = [x for x in candidates if x[i] == '1']
        else:
            candidates = [x for x in candidates if x[i] == '0']

        i += 1
    oxygen_generator = candidates[0]

    candidates = copy(report)
    i = 0
    while len(candidates) > 1:

        if sum([1 for row in candidates if row[i] == '1']) >= len(candidates) / 2:
            candidates = [x for x in candidates if x[i] == '0']
        else:
            candidates = [x for x in candidates if x[i] == '1']

        i += 1
    co2_scrubber = candidates[0]

    return oxygen_generator, co2_scrubber


if __name__ == '__main__':
    course = []

    with open('../data/3.txt') as f:
        report = [line.strip() for line in f]

    gamma, epsilon = get_indicators_part1(report)
    consumption = int(gamma, 2) * int(epsilon, 2)
    print(consumption)  # 2724524

    oxygen_generator, co2_scrubber = get_indicators_part2(report)
    life_support_rating = int(oxygen_generator, 2) * int(co2_scrubber, 2)
    print(life_support_rating)  # 2775870
