# https://adventofcode.com/2021/day/14

from typing import Dict, Tuple, List


def pair_insertion(rules: Dict[str, str], formula: Dict[str, int]) -> Dict[str, int]:
    new_formula = {}

    for pair, count in formula.items():
        new_pairs = [pair[0] + rules[pair], rules[pair] + pair[1]]

        for key in new_pairs:
            if new_formula.get(key):
                new_formula[key] += 1 * count
            else:
                new_formula[key] = count

    return new_formula


def apply_insertions(rules: Dict[str, str], formula: Dict[str, int], steps: int) -> Dict[str, int]:
    for i in range(steps):
        formula = pair_insertion(rules, formula)

    return formula


def count_elements(formula, first_letter, last_letter):
    counter = {}
    letters = [(k[0], v) for k, v in formula.items()] + [(k[1], v) for k, v in formula.items()]

    for letter in set([x[0] for x in letters]):
        if letter == first_letter or letter == last_letter:
            counter[letter] = (sum([x[1] for x in letters if x[0] == letter]) + 1) / 2
        else:
            counter[letter] = sum([x[1] for x in letters if x[0] == letter]) / 2

    least_common = int([(v) for k, v in counter.items() if v == min(counter.values())][0])
    most_common = int([(v) for k, v in counter.items() if v == max(counter.values())][0])

    return most_common, least_common


def parse_input(input: List[str]) -> Tuple[Dict[str, int], Dict[str, str]]:
    formula = {}
    rules = {}

    for pair in [input[0][i:i + 2] for i, x in enumerate(input[0][:-1])]:
        formula[pair] = 1

    for line in input[2:]:
        pair, value = line.split(' -> ')
        rules[pair] = value

    return formula, rules


if __name__ == '__main__':
    with open('../data/14.txt') as f:
        input = [x.strip() for x in f]

    formula, rules = parse_input(input)

    expanded_formula = apply_insertions(rules, formula, 10)
    most_common, least_common = count_elements(expanded_formula, input[0][0], input[0][-1])
    print(most_common - least_common)  # 2170

    expanded_formula = apply_insertions(rules, formula, 40)
    most_common, least_common = count_elements(expanded_formula, input[0][0], input[0][-1])
    print(most_common - least_common)  # 2422444761283
