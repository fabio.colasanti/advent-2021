# https://adventofcode.com/2021/day/11

from typing import List, Dict, Tuple


def parse_input(input: List[str]) -> Dict:
    octopuses = {}

    for y, line in enumerate(input):
        for x, value in enumerate(line):
            octopuses[(x, y)] = int(value)

    return octopuses


def get_neighbours(coord: Tuple[int, int]) -> List[Tuple[int, int]]:
    neighbours = []

    for x in range(max(0, coord[0] - 1), min(coord[0] + 1, 9) + 1):
        for y in range(max(0, coord[1] - 1), min(coord[1] + 1, 9) + 1):
            if (x, y) != coord:
                neighbours.append((x, y))

    return neighbours


def go_to_step(octopuses: Dict, steps: int) -> Tuple[Dict,int]:
    tot_flashed = 0
    step = 0

    while step < steps:
        octopuses = {k: v + 1 for k, v in octopuses.items()}

        flashed = [k for k, v in octopuses.items() if v == 10]

        for f in flashed:
            neighbours = get_neighbours(f)

            for n in neighbours:
                octopuses[n] += 1

            [flashed.append(neighbor) for neighbor in neighbours if octopuses[neighbor] == 10 and neighbor not in flashed]

        for k in flashed:
            octopuses[k] = 0
            tot_flashed += 1

        step += 1

    return octopuses, tot_flashed


def first_synchronised_flash(octopuses: Dict) -> int:
    step = 0

    while True:
        current, _ = go_to_step(octopuses, step)

        if len([x for x in current.values() if x == 0]) == 100:
            return step

        step += 1


if __name__ == '__main__':
    with open('../data/11.txt') as f:
        input = [x.strip() for x in f]

    octopuses = parse_input(input)

    _, flashed = go_to_step(octopuses, 100)
    print(flashed)  # 1601

    step = first_synchronised_flash(octopuses)
    print(step)  # 368

