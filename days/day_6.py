# https://adventofcode.com/2021/day/6

from collections import Counter
from typing import List
import numpy


def grow(school: List[int], days: int) -> List[int]:
    tot = 0

    for value, count in Counter(school).items():
        fish = numpy.array([value])

        for d in range(days):
            offspring = fish[fish == 0].size

            fish = fish - 1
            fish[fish < 0] = 6

            fish = numpy.concatenate((fish, numpy.array([8] * offspring)))

            print(d)

        tot += count * fish.size

    return tot


if __name__ == '__main__':
    with open('../data/6.txt') as f:
        school = [int(x) for x in [x.split(',') for x in f][0]]

    school_size = grow(school, 80)
    print(school_size)  # 375482

    school_size = grow(school, 256)
    print(school_size)  # 1689540415957
