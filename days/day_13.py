# https://adventofcode.com/2021/day/13

from typing import List, Dict, Tuple


def fold_paper(paper: Dict[Tuple[int, int], str], fold: int, axis: str) -> Dict[Tuple[int, int], str]:
    folded = {}

    for coord, value in paper.items():
        new_coord = coord

        if axis == 'x' and coord[0] > fold:
            new_coord = (fold * 2 - coord[0], coord[1])

        if axis == 'y' and coord[1] > fold:
            new_coord = (coord[0], fold * 2 - coord[1])

        folded[new_coord] = value

    return folded


def parse_input(input: List[str]) -> Tuple[Dict[Tuple[int, int], str], List[Tuple[str, int]]]:
    paper = {}
    instructions = []

    for line in input:
        if line == '':
            continue

        if line.startswith('fold'):
            line = line[11:]
            axis, fold = line.split('=')

            instructions.append((axis, int(fold)))
        else:
            x, y = line.split(',')
            paper[int(x), int(y)] = '#'

    return paper, instructions


if __name__ == '__main__':
    with open('../data/13.txt') as f:
        input = [x.strip() for x in f]

    paper, instructions = parse_input(input)

    for i, instruction in enumerate(instructions):
        fold = instruction[1]
        axis = instruction[0]

        paper = fold_paper(paper, fold, axis)

        if i == 0:
            print(len(paper))  # 765

        if i == len(instructions) - 1:
            width = max([x[0] for x in paper])
            height = max([x[1] for x in paper])

            for y in range(height + 1):
                print(''.join([str(paper.get((x, y), ' ')) for x in range(width + 1)]))  # RZKZLPGH

