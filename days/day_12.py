# https://adventofcode.com/2021/day/12
import copy
from typing import List, Dict


def parse_input(input: List[str]) -> Dict[str, List[int]]:
    caves_map = {}

    for x in input:
        k, v = x.split('-')
        values = caves_map.get(k, [])
        values.append(v)
        caves_map[k] = values

        values = caves_map.get(v, [])
        values.append(k)
        caves_map[v] = values

    for k, v in caves_map.items():
        if 'end' in v:
            end_index = v.index('end')
            caves_map[k] = v[:end_index] + v[end_index + 1:] + ['end']

    for k, v in caves_map.items():
        if 'start' in v:
            end_index = v.index('start')
            caves_map[k] = v[:end_index] + v[end_index + 1:]

    return caves_map


def get_paths(cave_map, cave, paths, current_path, part):
    available_caves = []

    current_path.append(cave)

    if part == 1:
        available_caves = [c for c in cave_map[cave] if c not in current_path or c.isupper()]

    if part == 2:
        available_caves = []

        for c in cave_map[cave]:
            if c.isupper():
                available_caves.append(c)
            else:
                if len([x for x in current_path if x == c]) == 0:
                    available_caves.append(c)
                else:
                    if len([x for x in current_path if x.islower()]) == len(set([x for x in current_path if x.islower()])):
                        available_caves.append(c)

    if available_caves:
        for cave in available_caves:

            if cave == 'end':
                current_path.append(cave)
                return current_path

            else:
                paths.append(get_paths(cave_map, cave, paths, copy.copy(current_path), part))

    return current_path


if __name__ == '__main__':
    with open('../data/12.txt') as f:
        input = [x.strip() for x in f]

    caves_map = parse_input(input)

    paths = []
    get_paths(caves_map, 'start', paths, [], 1)
    valid_paths = [path for path in paths if path[-1] == 'end']
    print(len(valid_paths))  # 5228

    paths = []
    get_paths(caves_map, 'start', paths, [], 2)
    valid_paths = [path for path in paths if path[-1] == 'end']
    print(len(valid_paths))  # 131228
