# https://adventofcode.com/2021/day/7

from typing import List


def min_consumption(positions: List[int], part) -> int:
    consumptions = []

    for i in range(max(positions)):
        consumptions.append(sum([get_consumption(i, position, part) for position in positions]))

    return min(consumptions)


def get_consumption(position, target, part):
    if part == 1:
        return abs(position - target)

    if part == 2:
        return sum([x for x in range(abs(position - target) + 1)])


if __name__ == '__main__':
    with open('../data/7.txt') as f:
        positions = [int(x) for x in [x.split(',') for x in f][0]]

    consumptions = min_consumption(positions, 1)
    print(min(consumptions))  # 348664

    consumptions = min_consumption(positions, 2)
    print(min(consumptions))  # 100220525
