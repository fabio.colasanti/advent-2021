# https://adventofcode.com/2021/day/4

from typing import List


class Board:
    def __init__(self, board_numbers: List[List[int]]):
        self.numbers = []
        self.completed = False

        for row, values in enumerate(board_numbers):
            for col, value in enumerate(values):
                self.numbers.append({
                    'row': row,
                    'col': col,
                    'number': value,
                    'drawn': False
                })

    def mark_number(self, number: int):
        for x in self.numbers:
            if x['number'] == number:
                x['drawn'] = True

    def bingo(self):
        for i in range(5):
            row = [x['number'] for x in self.numbers if x['row'] == i and x['drawn'] is True]
            col = [x['number'] for x in self.numbers if x['col'] == i and x['drawn'] is True]

            if len(row) == 5 or len(col) == 5:
                return True

        return False

    def score(self, number: int):
        uncalled = [x['number'] for x in self.numbers if x['drawn'] is False]

        return number * sum(uncalled)


def draw(draw_numbers: List[int], boards: List[Board]) -> List[dict]:
    winning_order = []

    for number in draw_numbers:
        for i, board in enumerate(boards):
            if not board.completed:
                board.mark_number(number)

                if board.bingo():
                    winning_order.append({'board': i, 'score': board.score(number)})
                    board.completed = True

    return winning_order


if __name__ == '__main__':
    with open('../data/4.txt') as f:
        draw_numbers = [int(x) for x in f.readline().split(',')]
        board_numbers = []
        boards = []

        for x in f.readlines():
            if x != '\n':
                board_numbers.append([int(a) for a in x.strip().replace('  ', ' ').split(' ')])

                if len(board_numbers) == 5:
                    board = Board(board_numbers)
                    boards.append(board)

                    board_numbers = []

    winning_order = draw(draw_numbers, boards)

    print(winning_order[0]['score'])  # 38913
    print(winning_order[-1]['score'])  # 16836
