# https://adventofcode.com/2021/day/2

from typing import List, Tuple


def move(position: Tuple[int, int, int], direction: str, distance: int, mode: str) -> Tuple[int, int, int]:
    x = position[0]
    y = position[1]
    aim = position[2]

    if mode == 'basic':
        if direction == 'forward':
            return x + distance, y, aim

        if direction == 'up':
            return x, y + distance, aim

        if direction == 'down':
            return x, y - distance, aim

    if mode == 'advanced':
        if direction == 'forward':
            return x + distance, y + aim * distance, aim

        if direction == 'up':
            return x, y, aim + distance

        if direction == 'down':
            return x, y, aim - distance


def follow_course(starting_position: Tuple[int, int, int], course: List[Tuple[str, int]], moveset: str) -> Tuple[int, int, int]:
    current_coords = starting_position

    for instruction in course:
        direction = instruction[0]
        distance = instruction[1]

        current_coords = move(current_coords, direction, distance, moveset)

    return current_coords


if __name__ == '__main__':
    course = []

    with open('../data/2.txt') as f:
        for line in f:
            direction, distance = line.strip().split(' ')
            course.append((direction, int(distance)))

    destination = follow_course((0, 0, 0), course, 'basic')
    print(destination[0] * destination[1])  # 1507611

    destination = follow_course((0, 0, 0), course, 'advanced')
    print(destination[0] * destination[1])  # 1880593125
