# https://adventofcode.com/2021/day/1

from typing import List


def count_increases(depths: List[int], window_size: int):
    increases = 0

    for i, x in enumerate(depths):
        if i > 0 and sum(depths[i:i + window_size]) > sum(depths[i - 1:i - 1 + window_size]):
            increases += 1

    return increases


if __name__ == '__main__':
    with open('../data/1.txt') as f:
        depths = [int(line) for line in f]

    depth_increases = count_increases(depths, 1)
    print(depth_increases)  # 1451

    depth_increases = count_increases(depths, 3)
    print(depth_increases)  # 1395
