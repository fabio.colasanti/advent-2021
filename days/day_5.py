# https://adventofcode.com/2021/day/5

from typing import List, Tuple, Dict


def parse_lines(lines: List[str]) -> List[List[Tuple[int, int]]]:
    coords = []

    for line in lines:
        ends = line.split(' -> ')

        x1, y1 = tuple([int(point) for point in ends[0].split(',')])
        x2, y2 = tuple([int(point) for point in ends[1].split(',')])

        sign_x = get_step(x1, x2)
        sign_y = get_step(y1, y2)

        tot_points = max(abs(x1 - x2), abs(y1 - y2)) + 1

        coords.append([(x1 + i * sign_x, y1 + i * sign_y) for i in range(tot_points)])

    return coords


def get_vents_map(coords: List[List[Tuple[int, int]]], include_diagonals: bool) -> Dict:
    vents_map = {}

    for line in coords:
        if is_straight(line) or include_diagonals:
            for point in line:
                vents_map[point] = vents_map.get(point, 0) + 1

    return vents_map


def get_step(value1: int, value2: int) -> int:
    if value1 == value2:
        return 0

    if value1 < value2:
        return 1

    if value1 > value2:
        return -1


def is_straight(line: List[Tuple[int, int]]) -> bool:
    x1 = line[0][0]
    y1 = line[0][1]

    x2 = line[-1][0]
    y2 = line[-1][1]

    return x1 == x2 or y1 == y2


if __name__ == '__main__':
    with open('../data/5.txt') as f:
        raw_lines = [x.strip() for x in f]

    lines = parse_lines(raw_lines)

    vents_map_excluding_diagonals = get_vents_map(lines, False)
    overlapping = len([x for x in vents_map_excluding_diagonals.values() if x > 1])
    print(overlapping)  # 7297

    vents_map_including_diagonals = get_vents_map(lines, True)
    overlapping = len([x for x in vents_map_including_diagonals.values() if x > 1])
    print(overlapping)  # 21038
