import pytest

from days.day_7 import get_consumption, min_consumption


@pytest.mark.parametrize('position, target, expected', [
    (16, 2, 14),
    (1, 2, 1),
    (2, 2, 0),
    (0, 2, 2),
    (4, 2, 2),
    (7, 2, 5),
    (14, 2, 12)
])
def test_get_consumption_part1(position, target, expected):
    actual = get_consumption(position, target, 1)

    assert actual == expected


@pytest.mark.parametrize('position, target, expected', [
    (16, 5, 66),
    (1, 5, 10),
    (2, 5, 6),
    (0, 5, 15),
    (4, 5, 1),
    (7, 5, 3),
    (14, 5, 45)
])
def test_get_consumption_part2(position, target, expected):
    actual = get_consumption(position, target, 2)

    assert actual == expected


@pytest.mark.parametrize('part, expected',[
    (1, 37),
    (2, 168)
])
def test_min_consumption(part, expected):
    positions = [16, 1, 2, 0, 4, 2, 7, 1, 2, 14]

    actual = min_consumption(positions, part)

    assert actual == expected
