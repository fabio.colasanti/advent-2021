import pytest

from days.day_9 import get_neighbouring_values, get_min_heights, parse_input, get_basin

heightmap = {
    (0, 0): 2, (1, 0): 1, (2, 0): 9, (3, 0): 9, (4, 0): 9, (5, 0): 4, (6, 0): 3, (7, 0): 2, (8, 0): 1, (9, 0): 0,
    (0, 1): 3, (1, 1): 9, (2, 1): 8, (3, 1): 7, (4, 1): 8, (5, 1): 9, (6, 1): 4, (7, 1): 9, (8, 1): 2, (9, 1): 1,
    (0, 2): 9, (1, 2): 8, (2, 2): 5, (3, 2): 6, (4, 2): 7, (5, 2): 8, (6, 2): 9, (7, 2): 8, (8, 2): 9, (9, 2): 2,
    (0, 3): 8, (1, 3): 7, (2, 3): 6, (3, 3): 7, (4, 3): 8, (5, 3): 9, (6, 3): 6, (7, 3): 7, (8, 3): 8, (9, 3): 9,
    (0, 4): 9, (1, 4): 8, (2, 4): 9, (3, 4): 9, (4, 4): 9, (5, 4): 6, (6, 4): 5, (7, 4): 6, (8, 4): 7, (9, 4): 8
}


@pytest.mark.parametrize('coord, expected', [
    ((2, 3), [(1, 3), (2, 2), (2, 4), (3, 3)]),
    ((0, 0), [(0, 1), (1, 0)]),
    ((0, 2), [(0, 1), (0, 3), (1, 2)]),
    ((9, 3), [(8, 3), (9, 2), (9, 4)]),
    ((5, 0), [(4, 0), (5, 1), (6, 0)]),
    ((4, 4), [(3, 4), (4, 3), (5, 4)]),
    ((0, 4), [(0, 3), (1, 4)]),
    ((9, 0), [(8, 0), (9, 1)]),
    ((9, 4), [(8, 4), (9, 3)]),
])
def test_get_neighbouring_values(coord, expected):
    actual = get_neighbouring_values(coord, heightmap)

    assert sorted(actual) == sorted(expected)


def test_get_min_heights():
    expected_min_heights_coords = [(1, 0), (9, 0), (2, 2), (6, 4)]
    expected_min_heights_values = [2, 1, 6, 6]

    min_heights_coords, min_heights_values = get_min_heights(heightmap)

    assert min_heights_coords == expected_min_heights_coords
    assert min_heights_values == expected_min_heights_values


def test_parse_heightmap():
    input = ['2199943210',
             '3987894921',
             '9856789892',
             '8767896789',
             '9899965678']
    expected = heightmap

    actual = parse_input(input)

    assert sorted(actual) == sorted(expected)


@pytest.mark.parametrize('coord, expected', [
    ((0, 0), [2, 1, 3]),
    ((9, 0), [4, 3, 2, 1, 0, 4, 2, 1, 2]),
    ((2, 2), [8, 7, 8, 8, 5, 6, 7, 8, 8, 7, 6, 7, 8, 8]),
    ((6, 4), [8, 6, 7, 8, 6, 5, 6, 7, 8])
])
def test_get_basin(coord, expected):
    actual = get_basin(coord, heightmap)

    assert sorted(actual) == sorted(expected)
