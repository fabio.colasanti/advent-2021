import pytest

from days.day_2 import move, follow_course


@pytest.mark.parametrize('position, direction, distance, expected', [
    ((0, 0, 0), 'forward', 5, (5, 0, 0)),
    ((5, 0, 0), 'down', 5, (5, -5, 0)),
    ((5, -5, 0), 'forward', 8, (13, -5, 0)),
    ((13, -5, 0), 'up', 3, (13, -2, 0)),
    ((13, -2, 0), 'down', 8, (13, -10, 0)),
    ((13, -10, 0), 'forward', 2, (15, -10, 0))
])
def test_move_basic(position, direction, distance, expected):
    actual = move(position, direction, distance, 'basic')

    assert actual == expected


@pytest.mark.parametrize('position, direction, distance, expected', [
    ((0, 0, 0), 'forward', 5, (5, 0, 0)),
    ((5, 0, 0), 'down', 5, (5, 0, -5)),
    ((5, 0, -5), 'forward', 8, (13, -40, -5)),
    ((13, -40, -5), 'up', 3, (13, -40, -2)),
    ((13, -40, -2), 'down', 8, (13, -40, -10)),
    ((13, -40, -10), 'forward', 2, (15, -60, -10))
])
def test_move_advanced(position, direction, distance, expected):
    actual = move(position, direction, distance, 'advanced')

    assert actual == expected


@pytest.mark.parametrize('moveset, expected', [
    ('basic', (1, 2, 0)),
    ('advanced', (1, 0, 2))
])
def test_follow_course(moveset, expected):
    starting_position = (0, 0, 0)
    course = [('forward', 1), ('up', 3), ('down', 1)]

    actual = follow_course(starting_position, course, moveset)

    assert actual == expected
