import pytest

from days.day_3 import get_indicators_part1, get_indicators_part2

report = ['00100',
          '11110',
          '10110',
          '10111',
          '10101',
          '01111',
          '00111',
          '11100',
          '10000',
          '11001',
          '00010',
          '01010']


def test_get_indicators_part1():
    expected = ('10110', '01001')
    actual = get_indicators_part1(report)

    assert actual == expected


def test_get_indicators_part2():
    expected = ('10111', '01010')
    actual = get_indicators_part2(report)

    assert actual == expected
