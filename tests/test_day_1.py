import pytest

from days.day_1 import count_increases


@pytest.mark.parametrize('window, expected', [
    (1, 7),
    (3, 5),
])
def test_count_increases(window, expected):
    depths = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263]

    actual = count_increases(depths, window)

    assert actual == expected
