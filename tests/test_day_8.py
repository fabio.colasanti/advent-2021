import pytest

from days.day_8 import count_easy_digits, map_signals_to_digits, decode_output, decode_all


@pytest.mark.parametrize('output, expected', [
    (['fdgacbe', 'cefdb', 'cefbgd', 'gcbe'], 2),
    (['fcgedb', 'cgb', 'dgebacf', 'gc'], 3),
    (['cg', 'cg', 'fdcagb', 'cbg'], 3),
    (['efabcd', 'cedba', 'gadfec', 'cb'], 1),
    (['gecf', 'egdcabf', 'bgf', 'bfgea'], 3),
    (['gebdcfa', 'ecba', 'ca', 'fadegcb'], 4),
    (['cefg', 'dcbef', 'fcge', 'gbcadfe'], 3),
    (['ed', 'bcgafe', 'cdgba', 'cbgef'], 1),
    (['gbdfcae', 'bgc', 'cg', 'cgb'], 4),
    (['fgae', 'cfgab', 'fg', 'bagce'], 2),
])
def test_count_easy_digits(output, expected):
    actual = count_easy_digits(output)

    assert actual == expected


@pytest.mark.parametrize('signal, expected', [
    (['be', 'cfbegad', 'cbdgef', 'fgaecd', 'cgeb', 'fdcge', 'agebfd', 'fecdb', 'fabcd', 'edb'],
     {0: {'b', 'a', 'd', 'f', 'e', 'g'},
      1: {'b', 'e'},
      2: {'c', 'b', 'a', 'd', 'f'},
      3: {'c', 'b', 'd', 'f', 'e'},
      4: {'b', 'c', 'e', 'g'},
      5: {'c', 'd', 'f', 'e', 'g'},
      6: {'c', 'a', 'd', 'f', 'e', 'g'},
      7: {'b', 'd', 'e'},
      8: {'c', 'b', 'a', 'd', 'f', 'e', 'g'},
      9: {'c', 'b', 'd', 'f', 'e', 'g'}}),

    (['edbfga', 'begcd', 'cbg', 'gc', 'gcadebf', 'fbgde', 'acbgfd', 'abcde', 'gfcbed', 'gfec'],
     {0: {'a', 'b', 'f', 'd', 'c', 'g'},
      1: {'c', 'g'},
      2: {'a', 'b', 'd', 'e', 'c'},
      3: {'b', 'd', 'e', 'c', 'g'},
      4: {'f', 'c', 'g', 'e'},
      5: {'b', 'd', 'e', 'f', 'g'},
      6: {'a', 'b', 'd', 'e', 'f', 'g'},
      7: {'c', 'b', 'g'},
      8: {'a', 'b', 'f', 'd', 'e', 'c', 'g'},
      9: {'b', 'd', 'c', 'e', 'f', 'g'}}),

    (['fgaebd', 'cg', 'bdaec', 'gdafb', 'agbcfd', 'gdcbef', 'bgcad', 'gfac', 'gcb', 'cdgabef'],
     {0: {'d', 'g', 'f', 'c', 'b', 'e'},
      1: {'c', 'g'},
      2: {'d', 'c', 'b', 'e', 'a'},
      3: {'d', 'g', 'c', 'b', 'a'},
      4: {'f', 'c', 'a', 'g'},
      5: {'d', 'g', 'f', 'b', 'a'},
      6: {'d', 'g', 'f', 'b', 'e', 'a'},
      7: {'c', 'b', 'g'},
      8: {'d', 'g', 'f', 'c', 'b', 'e', 'a'},
      9: {'d', 'g', 'f', 'c', 'b', 'a'}}),
])
def test_map_signals_to_digits(signal, expected):
    actual = map_signals_to_digits(signal)

    assert actual == expected


@pytest.mark.parametrize('output, mapping, expected', [
    (['fdgacbe', 'cefdb', 'cefbgd', 'gcbe'],
     {0: {'b', 'a', 'd', 'f', 'e', 'g'},
      1: {'b', 'e'},
      2: {'c', 'b', 'a', 'd', 'f'},
      3: {'c', 'b', 'd', 'f', 'e'},
      4: {'b', 'c', 'e', 'g'},
      5: {'c', 'd', 'f', 'e', 'g'},
      6: {'c', 'a', 'd', 'f', 'e', 'g'},
      7: {'b', 'd', 'e'},
      8: {'c', 'b', 'a', 'd', 'f', 'e', 'g'},
      9: {'c', 'b', 'd', 'f', 'e', 'g'}},
     8394),
    (['fcgedb', 'cgb', 'dgebacf', 'gc'],
     {0: {'a', 'b', 'f', 'd', 'c', 'g'},
      1: {'c', 'g'},
      2: {'a', 'b', 'd', 'e', 'c'},
      3: {'b', 'd', 'e', 'c', 'g'},
      4: {'f', 'c', 'g', 'e'},
      5: {'b', 'd', 'e', 'f', 'g'},
      6: {'a', 'b', 'd', 'e', 'f', 'g'},
      7: {'c', 'b', 'g'},
      8: {'a', 'b', 'f', 'd', 'e', 'c', 'g'},
      9: {'b', 'd', 'c', 'e', 'f', 'g'}},
     9781),
    (['cg', 'cg', 'fdcagb', 'cbg'],
     {0: {'d', 'g', 'f', 'c', 'b', 'e'},
      1: {'c', 'g'},
      2: {'d', 'c', 'b', 'e', 'a'},
      3: {'d', 'g', 'c', 'b', 'a'},
      4: {'f', 'c', 'a', 'g'},
      5: {'d', 'g', 'f', 'b', 'a'},
      6: {'d', 'g', 'f', 'b', 'e', 'a'},
      7: {'c', 'b', 'g'},
      8: {'d', 'g', 'f', 'c', 'b', 'e', 'a'},
      9: {'d', 'g', 'f', 'c', 'b', 'a'}},
     1197)
])
def test_decode_output(output, mapping, expected):
    actual = decode_output(output, mapping)

    assert actual == expected


def test_decode_all():
    signals = [
        ['be', 'cfbegad', 'cbdgef', 'fgaecd', 'cgeb', 'fdcge', 'agebfd', 'fecdb', 'fabcd', 'edb'],
        ['edbfga', 'begcd', 'cbg', 'gc', 'gcadebf', 'fbgde', 'acbgfd', 'abcde', 'gfcbed', 'gfec'],
        ['fgaebd', 'cg', 'bdaec', 'gdafb', 'agbcfd', 'gdcbef', 'bgcad', 'gfac', 'gcb', 'cdgabef']
    ]
    outputs = [
        ['fdgacbe', 'cefdb', 'cefbgd', 'gcbe'],
        ['fcgedb', 'cgb', 'dgebacf', 'gc'],
        ['cg', 'cg', 'fdcagb', 'cbg']
    ]

    expected = [8394, 9781, 1197]

    actual = decode_all(signals, outputs)

    assert actual == expected

