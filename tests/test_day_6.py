import pytest

from days.day_6 import grow


@pytest.mark.parametrize('days, expected', [
    (18, 26),
    (80, 5934),
    # (256, 26984457539)
])
def test_school_size(days, expected):
    school = [3, 4, 3, 1, 2]

    school = grow(school, days)

    assert school == expected

