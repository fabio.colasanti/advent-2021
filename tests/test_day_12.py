import pytest

from days.day_12 import parse_input, get_paths

caves_map = {
    'start': ['A', 'b'],
    'A': ['c', 'b', 'end'],
    'b': ['A', 'd', 'end'],
    'c': ['A'],
    'd': ['b'],
    'end': ['A', 'b']
}


def test_parse_input():
    input = [
        'start-A',
        'start-b',
        'A-c',
        'A-b',
        'A-end',
        'b-end',
        'b-d',
    ]

    expected = caves_map

    actual = parse_input(input)

    assert actual == expected


def test_get_paths_part_1():
    paths = []

    expected = [
        ['start', 'A', 'b', 'A', 'c', 'A', 'end'],
        ['start', 'A', 'b', 'A', 'end'],
        ['start', 'A', 'b', 'end'],
        ['start', 'A', 'c', 'A', 'b', 'A', 'end'],
        ['start', 'A', 'c', 'A', 'b', 'end'],
        ['start', 'A', 'c', 'A', 'end'],
        ['start', 'A', 'end'],
        ['start', 'b', 'A', 'c', 'A', 'end'],
        ['start', 'b', 'A', 'end'],
        ['start', 'b', 'end']
    ]

    get_paths(caves_map, 'start', paths, [], 1)

    assert sorted([path for path in paths if path[-1] == 'end']) == sorted(expected)


def test_get_paths_part_2():
    paths = []

    expected = [
        ['start', 'A', 'c', 'A', 'c', 'A', 'b', 'A', 'end'],
        ['start', 'A', 'c', 'A', 'c', 'A', 'b', 'end'],
        ['start', 'A', 'c', 'A', 'c', 'A', 'end'],
        ['start', 'A', 'c', 'A', 'b', 'A', 'c', 'A', 'end'],
        ['start', 'A', 'c', 'A', 'b', 'A', 'b', 'A', 'end'],
        ['start', 'A', 'c', 'A', 'b', 'A', 'b', 'end'],
        ['start', 'A', 'c', 'A', 'b', 'A', 'end'],
        ['start', 'A', 'c', 'A', 'b', 'd', 'b', 'A', 'end'],
        ['start', 'A', 'c', 'A', 'b', 'd', 'b', 'end'],
        ['start', 'A', 'c', 'A', 'b', 'end'],
        ['start', 'A', 'c', 'A', 'end'],
        ['start', 'A', 'b', 'A', 'c', 'A', 'c', 'A', 'end'],
        ['start', 'A', 'b', 'A', 'c', 'A', 'b', 'A', 'end'],
        ['start', 'A', 'b', 'A', 'c', 'A', 'b', 'end'],
        ['start', 'A', 'b', 'A', 'c', 'A', 'end'],
        ['start', 'A', 'b', 'A', 'b', 'A', 'c', 'A', 'end'],
        ['start', 'A', 'b', 'A', 'b', 'A', 'end'],
        ['start', 'A', 'b', 'A', 'b', 'end'],
        ['start', 'A', 'b', 'A', 'end'],
        ['start', 'A', 'b', 'd', 'b', 'A', 'c', 'A', 'end'],
        ['start', 'A', 'b', 'd', 'b', 'A', 'end'],
        ['start', 'A', 'b', 'd', 'b', 'end'],
        ['start', 'A', 'b', 'end'],
        ['start', 'A', 'end'],
        ['start', 'b', 'A', 'c', 'A', 'c', 'A', 'end'],
        ['start', 'b', 'A', 'c', 'A', 'b', 'A', 'end'],
        ['start', 'b', 'A', 'c', 'A', 'b', 'end'],
        ['start', 'b', 'A', 'c', 'A', 'end'],
        ['start', 'b', 'A', 'b', 'A', 'c', 'A', 'end'],
        ['start', 'b', 'A', 'b', 'A', 'end'],
        ['start', 'b', 'A', 'b', 'end'],
        ['start', 'b', 'A', 'end'],
        ['start', 'b', 'd', 'b', 'A', 'c', 'A', 'end'],
        ['start', 'b', 'd', 'b', 'A', 'end'],
        ['start', 'b', 'd', 'b', 'end'],
        ['start', 'b', 'end']
    ]

    get_paths(caves_map, 'start', paths, [], 2)

    assert sorted([path for path in paths if path[-1] == 'end']) == sorted(expected)
