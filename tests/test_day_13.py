import pytest

from days.day_13 import fold_paper, parse_input

paper = {
    (0, 13): '#',
    (0, 14): '#',
    (0, 3): '#',
    (1, 10): '#',
    (2, 14): '#',
    (3, 0): '#',
    (3, 4): '#',
    (4, 1): '#',
    (4, 11): '#',
    (6, 0): '#',
    (6, 10): '#',
    (6, 12): '#',
    (8, 10): '#',
    (8, 4): '#',
    (9, 0): '#',
    (9, 10): '#',
    (10, 12): '#',
    (10, 4): '#',
}


def test_fold_along_y():
    fold = 7
    axis = 'y'

    expected = {
        (0, 0): '#',
        (0, 1): '#',
        (0, 3): '#',
        (1, 4): '#',
        (2, 0): '#',
        (3, 0): '#',
        (3, 4): '#',
        (4, 1): '#',
        (4, 3): '#',
        (6, 0): '#',
        (6, 2): '#',
        (6, 4): '#',
        (8, 4): '#',
        (9, 0): '#',
        (9, 4): '#',
        (10, 2): '#',
        (10, 4): '#',
    }

    actual = fold_paper(paper, fold, axis)

    assert actual == expected


def test_fold_along_x():
    fold = 5
    axis = 'x'

    paper = {
        (0, 0): '#',
        (0, 1): '#',
        (0, 3): '#',
        (1, 4): '#',
        (2, 0): '#',
        (3, 0): '#',
        (3, 4): '#',
        (4, 1): '#',
        (4, 3): '#',
        (6, 0): '#',
        (6, 2): '#',
        (6, 4): '#',
        (8, 4): '#',
        (9, 0): '#',
        (9, 4): '#',
        (10, 2): '#',
        (10, 4): '#',
    }

    expected = {
        (0, 0): '#',
        (0, 1): '#',
        (0, 2): '#',
        (0, 3): '#',
        (0, 4): '#',
        (1, 0): '#',
        (1, 4): '#',
        (2, 0): '#',
        (2, 4): '#',
        (3, 0): '#',
        (3, 4): '#',
        (4, 0): '#',
        (4, 1): '#',
        (4, 2): '#',
        (4, 3): '#',
        (4, 4): '#'
    }

    actual = fold_paper(paper, fold, axis)

    assert actual == expected


def test_parse_input():
    input = [
        '6,10',
        '0,14',
        '9,10',
        '0,3',
        '10,4',
        '4,11',
        '6,0',
        '6,12',
        '4,1',
        '0,13',
        '10,12',
        '3,4',
        '3,0',
        '8,4',
        '1,10',
        '2,14',
        '8,10',
        '9,0',
        '',
        'fold along y=7',
        'fold along x=5'
    ]

    expected_paper = paper
    expected_instructions = [('y', 7), ('x', 5)]

    actual_paper, actual_instructions = parse_input(input)

    assert actual_paper == expected_paper
    assert actual_instructions == expected_instructions
