import pytest

from days.day_14 import pair_insertion, count_elements, parse_input

rules = {
    'CH': 'B',
    'HH': 'N',
    'CB': 'H',
    'NH': 'C',
    'HB': 'C',
    'HC': 'B',
    'HN': 'C',
    'NN': 'C',
    'BH': 'H',
    'NC': 'B',
    'NB': 'B',
    'BN': 'B',
    'BB': 'N',
    'BC': 'B',
    'CC': 'N',
    'CN': 'C'
}

formula = {
    'NN': 1,
    'NC': 1,
    'CB': 1
}


@pytest.mark.parametrize('formula, expected', [
    ({'NN': 1, 'NC': 1, 'CB': 1}, {'BC': 1, 'CH': 1, 'CN': 1, 'HB': 1, 'NB': 1, 'NC': 1}),
    ({'BC': 1, 'CH': 1, 'CN': 1, 'HB': 1, 'NB': 1, 'NC': 1}, {'BB': 2, 'BC': 2, 'BH': 1, 'CB': 2, 'CC': 1, 'CN': 1, 'HC': 1, 'NB': 2}),
    ({'BB': 2, 'BC': 2, 'BH': 1, 'CB': 2, 'CC': 1, 'CN': 1, 'HC': 1, 'NB': 2}, {'BB': 4, 'BC': 3, 'BH': 1, 'BN': 2, 'CC': 1, 'CH': 2, 'CN': 2, 'HB': 3, 'HH': 1, 'NB': 4, 'NC': 1}),
    ({'BB': 4, 'BC': 3, 'BH': 1, 'BN': 2, 'CC': 1, 'CH': 2, 'CN': 2, 'HB': 3, 'HH': 1, 'NB': 4, 'NC': 1},
     {'BB': 9, 'BC': 4, 'BH': 3, 'BN': 6, 'CB': 5, 'CC': 2, 'CN': 3, 'HC': 3, 'HH': 1, 'HN': 1, 'NB': 9, 'NC': 1, 'NH': 1})
])
def test_pair_insertion(formula, expected):
    actual = pair_insertion(rules, formula)

    assert actual == expected


def test_count_elements():
    first_letter = 'N'
    last_letter = 'B'
    formula = {'BB': 9, 'BC': 4, 'BH': 3, 'BN': 6, 'CB': 5, 'CC': 2, 'CN': 3, 'HC': 3, 'HH': 1, 'HN': 1, 'NB': 9, 'NC': 1, 'NH': 1}

    expected_most_common = 23
    expected_least_common = 5

    actual_most_common, actual_least_common = count_elements(formula, first_letter, last_letter)

    assert actual_most_common == expected_most_common
    assert actual_least_common == expected_least_common


def test_parse_input():
    input = [
        'NNCB',
        '',
        'CH -> B',
        'HH -> N',
        'CB -> H',
        'NH -> C',
        'HB -> C',
        'HC -> B',
        'HN -> C',
        'NN -> C',
        'BH -> H',
        'NC -> B',
        'NB -> B',
        'BN -> B',
        'BB -> N',
        'BC -> B',
        'CC -> N',
        'CN -> C'
    ]

    expected_formula = formula
    expected_rules = rules

    actual_formula, actual_rules = parse_input(input)

    assert actual_formula == expected_formula
    assert actual_rules == expected_rules
